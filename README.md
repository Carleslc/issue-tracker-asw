# Issue Tracker ASW

*Desenvolupat per:*

* Albert López Alcácer
* Alberto Pérez Torres
* Carlos Lázaro Costa
* David Segovia Tomàs

# Prova l'aplicació!

[Heroku server](https://issue-tracker-asw-fib.herokuapp.com/)

# Configuració en local
```bash
git clone https://bitbucket.org/dsegoviat/issue_tracker11.1.git
cd issue_tracker11.1
bundle update
bundle install
rails db:migrate
rails s
```
Després de configurar-ho accedeix a [http://localhost:3000](http://localhost:3000)

# Deploy a Heroku

Crea un compte de Heroku i una [nova app](https://dashboard.heroku.com/new-app) a Europa. Agafa les teves [credencials de AWS](https://devcenter.heroku.com/articles/s3#credentials). Crea un bucket de S3 a Irlanda (eu-west-1).

```bash
heroku login
heroku git:remote -a issue-tracker-asw-fib
heroku config:set AWS_ACCESS_KEY_ID=xxx AWS_SECRET_ACCESS_KEY=yyy
heroku config:set S3_BUCKET_NAME=issue-tracker-asw-fib
heroku config:set AWS_REGION=eu-west-1
git push heroku master
heroku run bundle exec rake db:migrate
```

Modifica `issue-tracker-asw-fib` amb el teus noms de l'app de Heroku i el bucket de S3 pels [arxius adjunts](https://devcenter.heroku.com/articles/paperclip-s3).