require 'test_helper'

class FileAttsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @file_att = file_atts(:one)
  end

  test "should get index" do
    get file_atts_url
    assert_response :success
  end

  test "should get new" do
    get new_file_att_url
    assert_response :success
  end

  test "should create file_att" do
    assert_difference('FileAtt.count') do
      post file_atts_url, params: { file_att: {  } }
    end

    assert_redirected_to file_att_url(FileAtt.last)
  end

  test "should show file_att" do
    get file_att_url(@file_att)
    assert_response :success
  end

  test "should get edit" do
    get edit_file_att_url(@file_att)
    assert_response :success
  end

  test "should update file_att" do
    patch file_att_url(@file_att), params: { file_att: {  } }
    assert_redirected_to file_att_url(@file_att)
  end

  test "should destroy file_att" do
    assert_difference('FileAtt.count', -1) do
      delete file_att_url(@file_att)
    end

    assert_redirected_to file_atts_url
  end
end
