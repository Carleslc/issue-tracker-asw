require 'test_helper'

class FattachmentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @fattachment = fattachments(:one)
  end

  test "should get index" do
    get fattachments_url
    assert_response :success
  end

  test "should get new" do
    get new_fattachment_url
    assert_response :success
  end

  test "should create fattachment" do
    assert_difference('Fattachment.count') do
      post fattachments_url, params: { fattachment: {  } }
    end

    assert_redirected_to fattachment_url(Fattachment.last)
  end

  test "should show fattachment" do
    get fattachment_url(@fattachment)
    assert_response :success
  end

  test "should get edit" do
    get edit_fattachment_url(@fattachment)
    assert_response :success
  end

  test "should update fattachment" do
    patch fattachment_url(@fattachment), params: { fattachment: {  } }
    assert_redirected_to fattachment_url(@fattachment)
  end

  test "should destroy fattachment" do
    assert_difference('Fattachment.count', -1) do
      delete fattachment_url(@fattachment)
    end

    assert_redirected_to fattachments_url
  end
end
