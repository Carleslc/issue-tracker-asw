class IssuesController < ApplicationController
  include Commentable
  before_action :set_issue, only: [:show, :edit, :update, :destroy]

  # GET /issues
  # GET /issues?filter=[open|responsible|watching]
  # GET /issues.json
  def index
    params[:filter] = session[:filter] unless params[:filter]
    @issues = case params[:filter]
                when 'open'
                  Issue.opened
                when 'responsible'
                  Issue.where(user_id: @current_user.id)
                when 'watching'
                  issues_array = []
                  Issue.all.each do |issue|
                    issue.current_user = current_user
                    issues_array << issue if issue.watched_by_current_user?
                  end
                  issues_array
                else
                  Issue.all
              end
    session[:filter] = params[:filter]
  end

  # GET /issues/1
  # GET /issues/1.json
  def show
  end

  # GET /issues/new
  def new
    @issue = Issue.new
  end
  

  # GET /issues/1/edit
  def edit
  end

  # POST /issues
  # POST /issues.json
  def create
    @issue = Issue.new(issue_params)
    respond_to do |format|
      if @issue.save
        
        if params[:uploads]
          params[:uploads].each { |upload|
            @issue.fattachments.create(file: upload)
          }
        end
        
        add_comment(@issue, 'created this issue', current_user.id)
        format.html { redirect_to @issue, notice: 'Issue was successfully created.' }
        format.json { render :show, status: :created, location: @issue }
      else
        format.html { render :new }
        format.json { render json: @issue.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /issues/1
  # PATCH/PUT /issues/1.json
  def update
    respond_to do |format|
      old = @issue.dup
      @issue.assign_attributes(issue_params)
      update_comment = set_update_comment
      if @issue.save
        
        if params[:uploads]
          params[:uploads].each { |upload|
            @issue.fattachments.create(file: upload)
          }
        end
        
        add_comment(@issue, update_comment, current_user.id) unless update_comment.empty?
        format.html { redirect_to @issue, notice: 'Issue was successfully updated.' }
        format.json { render :show, status: :ok, location: @issue }
      else
        format.html { render :edit }
        format.json { render json: @issue.errors, status: :unprocessable_entity }
        @issue = old
      end
    end
  end

  def set_update_comment
    update_comment = ''
    @issue.changed.each do |attr|
      key = attr.to_s
      value = @issue[attr].to_s
      value = 'any' if value.empty?
      if attr.to_s == 'status'
        value = humanize_status(value)
      elsif key == 'user_id'
        key = 'assignee'
        value = User.find_by_id(@issue[attr]).name if value != 'any'
      end
      update_comment += '* updated ' + key + ' to ' + value + "\n"
    end
    update_comment
  end

  # DELETE /issues/1
  # DELETE /issues/1.json
  def destroy
    @issue.destroy
    respond_to do |format|
      format.html { redirect_to issues_url, notice: 'Issue was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  helper_method :humanize_status

  def humanize_status(status)
    status.gsub('_issue', '').humanize
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_issue
      @issue = Issue.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def issue_params
      params.require(:issue).permit(:title, :description, :user_id, :kind, :priority, :status, :fileattachments)
    end
end
