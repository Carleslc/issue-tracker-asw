module Commentable
  extend ActiveSupport::Concern

  def add_comment(issue, comment, user_id)
    issue.comments.build(acomment: comment, user_id: user_id).save
  end
end