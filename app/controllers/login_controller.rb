class LoginController < ApplicationController
  skip_before_action :require_login

  def show
    redirect_to issues_path if current_user
  end
end
