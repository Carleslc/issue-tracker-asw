class VotesController < ApplicationController
  before_action :fetch_params

  def create
    if @issue.users_voting.exists? @user.id
      @issue.users_voting.destroy @user
      redirect_to @issue
    else
      @issue.users_voting << @user
      @issue.users_watching << @user unless @issue.users_watching.exists? @user.id
      redirect_to @issue, notice: 'Issue voted.'
    end
  end

  def fetch_params
    @issue = Issue.find_by_id!(params[:id])
    @user = current_user
  end
end
