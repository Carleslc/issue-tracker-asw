class FattachmentsController < ApplicationController
  before_action :set_fattachment, only: [:show, :edit, :update, :destroy]

  # GET /fattachments
  # GET /fattachments.json
  def index
    @fattachments = Fattachment.all
  end

  # GET /fattachments/1
  # GET /fattachments/1.json
  def show
  end

  # GET /fattachments/new
  def new
    @fattachment = Fattachment.new
  end

  # GET /fattachments/1/edit
  def edit
  end

  # POST /fattachments
  # POST /fattachments.json
  def create
    @fattachment = Fattachment.new(fattachment_params)

    respond_to do |format|
      if @fattachment.save
        format.html { redirect_to @fattachment, notice: 'Attachment was successfully added.' }
        format.json { render :show, status: :created, location: @fattachment }
      else
        format.html { render :new }
        format.json { render json: @fattachment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /fattachments/1
  # PATCH/PUT /fattachments/1.json
  def update
    respond_to do |format|
      if @fattachment.update(fattachment_params)
        format.html { redirect_to @fattachment, notice: 'Attachment was successfully updated.' }
        format.json { render :show, status: :ok, location: @fattachment }
      else
        format.html { render :edit }
        format.json { render json: @fattachment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /fattachments/1
  # DELETE /fattachments/1.json
  def destroy
    @fattachment.destroy
    respond_to do |format|
      format.html { redirect_to :back, notice: 'Attachment was successfully removed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_fattachment
      @fattachment = Fattachment.find(params[:id])
    end
    
    #

    # Never trust parameters from the scary internet, only allow the white list through.
    def fattachment_params
      params.fetch(:fattachment, {})
    end
end
