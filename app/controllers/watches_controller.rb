class WatchesController < ApplicationController
  before_action :fetch_params

  def create
    if @issue.users_watching.exists? @user.id
      @issue.users_watching.destroy @user
      redirect_to @issue
    else
      @issue.users_watching << @user
      redirect_to @issue, notice: 'You are now watching this issue.'
    end
  end

  def fetch_params
    @issue = Issue.find_by_id!(params[:id])
    @user = current_user
  end
end
