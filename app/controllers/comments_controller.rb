class CommentsController < ApplicationController
  include Commentable
  before_action :fetch_params

  def create
    if add_comment(@issue, @message, @user.id)
      redirect_to @issue, notice: 'Comment added.'
    else
      redirect_to @issue, alert: "Please, enter a valid comment"
    end
  end

  def fetch_params
    @user = current_user
    @issue = Issue.find_by_id! params[:id]
    @message = params[:comment][:acomment]
  end

end