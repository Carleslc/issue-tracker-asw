json.array!(@issues) do |issue|
  json.extract! issue, :id, :title, :description, :user_id, :kind, :priority
  json.url issue_url(issue, format: :json)
end
