json.array!(@file_atts) do |file_att|
  json.extract! file_att, :id
  json.url file_att_url(file_att, format: :json)
end
