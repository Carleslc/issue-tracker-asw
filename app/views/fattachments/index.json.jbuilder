json.array!(@fattachments) do |fattachment|
  json.extract! fattachment, :id
  json.url fattachment_url(fattachment, format: :json)
end
