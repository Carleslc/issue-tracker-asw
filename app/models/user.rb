class User < ApplicationRecord
  has_many :issues
  has_many :watches, dependent: :destroy
  has_many :watching_issues, through: :watches, source: :issue
  has_many :votes, dependent: :destroy
  has_many :voting_issues, through: :votes, source: :issue
  has_many :comments, dependent: :destroy
  has_many :commenting_issues, through: :comments, source: :issue

  validates :email, presence: true
  validates :name, presence: true
  validates :provider, presence: true
  validates :oauth_token, presence: true
  validates :oauth_expires_at, presence: true

  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_initialize.tap do |user|
      user.provider = auth.provider
      user.uid = auth.uid
      user.name = auth.info.name
      user.email = auth.info.email
      user.image = auth.info.image
      user.oauth_token = auth.credentials.token
      user.oauth_expires_at = Time.at(auth.credentials.expires_at)
      user.save!
    end
  end
end
