class Issue < ApplicationRecord
  belongs_to :user
  has_many :fattachments, :dependent => :destroy
  has_many :watches, dependent: :destroy
  has_many :users_watching, through: :watches, source: :user
  has_many :votes, dependent: :destroy
  has_many :users_voting, through: :votes, source: :user
  has_many :comments, dependent: :destroy
  has_many :users_commenting, through: :comments, source: :user
  enum kind: [:bug, :enhancement, :proposal, :task]
  enum priority: [:trivial, :minor, :major, :critical, :blocking]
  enum status: [:new_issue, :open, :on_hold, :resolved, :duplicate, :invalid_issue, :wont_fix, :closed]

  validates :title, presence: true
  validates :kind, presence: true
  validates :priority, presence: true
  validates :status, presence: true

  scope :opened, -> { where(status: ['new_issue', 'open']) }

  attr_accessor :current_user

  def watchers
    users_watching.size
  end

  def num_votes
    users_voting.size
  end

  def watched_by_current_user?
    users_watching.include? @current_user
  end

  def voted_by_current_user?
    users_voting.include? @current_user
  end
end
