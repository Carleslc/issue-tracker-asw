module IssuesHelper
  def confirm_issue_form_text(issue)
    if find_issue(issue); 'Update issue'
    else 'Create issue'
    end
  end

  def cancel_issue_form_path(issue)
    if find_issue(issue); issue
    else issues_path
    end
  end

  def active_filter(filter)
    if session[:filter]
      'active' if session[:filter] == filter
    else
      'active' if filter == 'all'
    end
  end

  private

  def find_issue(issue)
    Issue.find_by_id(issue.id)
  end
end
