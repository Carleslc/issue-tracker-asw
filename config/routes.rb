Rails.application.routes.draw do
  resources :fattachments
  resources :file_atts
  get 'attachments/index'

  get 'attachments/new'

  get 'attachments/create'

  get 'attachments/destroy'

  # Login
  root 'login#show'

  get 'sessions/create'
  get 'sessions/destroy'
  get 'login/show'

  get 'auth/:provider/callback', to: 'sessions#create'
  get 'auth/failure', to: redirect('/')
  get 'logout', to: 'sessions#destroy', as: 'logout'

  resources :sessions, only: [:create, :destroy]
  resource :login, only: [:show]

  # Users
  resources :users

  # Issues
  resources :issues

  post '/issues/:id/watch', to: 'watches#create', as: 'watch'
  post '/issues/:id/vote', to: 'votes#create', as: 'vote'
  
  # Attachments
  resources :attachments, only: [:index, :new, :create, :destroy]
  post '/issues/:id/comments', to: 'comments#create', as: 'issue_comments'

end
