class UniqueEmailToUsers < ActiveRecord::Migration[5.0][5.0]
  def change
    add_index :users, :email, :unique => true
  end
end
