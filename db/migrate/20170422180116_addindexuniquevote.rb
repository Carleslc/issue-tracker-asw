class Addindexuniquevote < ActiveRecord::Migration[5.0][5.0]
  def change
    add_index :votes, [:issue_id, :user_id], :unique => true
  end
end
