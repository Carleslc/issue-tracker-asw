class CreateFattachments < ActiveRecord::Migration[5.0][5.0]
  def change
    create_table :fattachments do |t|
      t.belongs_to :issue, index: true
      t.timestamps
    end
  end
end
