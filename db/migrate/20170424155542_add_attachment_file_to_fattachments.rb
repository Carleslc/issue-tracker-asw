class AddAttachmentFileToFattachments < ActiveRecord::Migration[5.0]
  def self.up
    change_table :fattachments do |t|
      t.attachment :file
    end
  end

  def self.down
    remove_attachment :fattachments, :file
  end
end
