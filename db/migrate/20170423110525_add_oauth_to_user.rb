class AddOauthToUser < ActiveRecord::Migration[5.0][5.0]
  def change
    add_column :users, :uid, :string
    add_index :users, :uid, unique: true
    add_column :users, :provider, :string
    add_column :users, :oauth_token, :string
    add_column :users, :oauth_expires_at, :datetime
  end
end
