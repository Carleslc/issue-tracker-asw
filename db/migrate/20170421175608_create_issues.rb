class CreateIssues < ActiveRecord::Migration[5.0][5.0]
  def change
    create_table :issues do |t|
      t.string :title
      t.string :description
      t.references :user, foreign_key: true, null: true

      t.timestamps
    end
    add_index :issues, [:user_id, :created_at, :updated_at]
  end
end
