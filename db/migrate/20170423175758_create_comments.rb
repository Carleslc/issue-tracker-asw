class CreateComments < ActiveRecord::Migration[5.0][5.0]
  def change
    create_table :comments do |t|
      t.belongs_to :user, index: true
      t.belongs_to :issue, index: true
      t.text :acomment
      t.timestamps
    end
  end
end
